<?php
	session_start();

	require_once "core/include.functions.php";
	include_once "core/entity/Client.php";

	if ($_SERVER['REQUEST_URI'] == "/")
	{
		$page = 'home';
	}
	else
	{
		$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		$urn_parts = explode('/', trim($url_path, '/'));
		$page = array_shift($urn_parts);
	}

	switch ($page)
	{
		case 'home':
			include "pages/home.phtml";
			break;
		case 'new-waybill':
			include "pages/createWaybill.phtml";
			break;
		case 'sign-in':
			include "pages/signIn.phtml";
			break;
		case 'sign-up':
			include "pages/signUp.phtml";
			break;
		case 'add-card':
			include "pages/addCard.phtml";
			break;
		case 'my-waybills':
			include "pages/myWaybills.phtml";
			break;
		case 'action': 
			include "core/formsRoute.php";
			break;
		default:
			die('error 404<br>'.$page);
			break;
	}
?>