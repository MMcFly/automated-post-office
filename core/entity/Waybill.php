<?php
class Waybill
{
	private $id;
	private $destination_id;
	private $receiver_id;
	private $shipping_price;
	private $sent_date;
	private $arrival_date;
	private $delivery_type;
	private $delivery_payer;
	private $delivery_status;
	private $payment_status;
	private $fk_client;
	private $fk_post_office;

	function __construct($builder)
	{
		$this->id = $builder->id;
        $this->destination_id = $builder->destination_id;
        $this->receiver_id = $builder->receiver_id;
        $this->shipping_price = $builder->shipping_price;
        $this->sent_date = $builder->sent_date;
        $this->arrival_date = $builder->arrival_date;
        $this->delivery_type = $builder->delivery_type;
        $this->delivery_payer = $builder->delivery_payer;
        $this->delivery_status = $builder->delivery_status;
        $this->payment_status = $builder->payment_status;
        $this->fk_client = $builder->fk_client;
        $this->fk_post_office = $builder->fk_post_office;
	}
	public function getId(){
		return $this->id;
	}
	public function setId($value){
		$this->id = $value;
	}

	public function getDestination_id(){
		return $this->destination_id;
	}
	public function setDestination_id($value){
		$this->destination_id = $value;
	}

	public function getReceiver_id(){
		return $this->receiver_id;
	}
	public function setReceiver_id($value){
		$this->receiver_id = $value;
	}

	public function getShipping_price(){
		return $this->shipping_price;
	}
	public function setShipping_price($value){
		$this->shipping_price = $value;
	}

	public function getSent_date(){
		return $this->sent_date;
	}
	public function setSent_date($value){
		$this->sent_date = $value;
	}

	public function getArrival_date(){
		return $this->arrival_date;
	}
	public function setArrival_date($value){
		$this->arrival_date = $value;
	}

	public function getDelivery_type(){
		return $this->delivery_type;
	}
	public function setDelivery_type($value){
		$this->delivery_type = $value;
	}

	public function getDelivery_payer(){
		return $this->delivery_payer;
	}
	public function setDelivery_payer($value){
		$this->delivery_payer = $value;
	}

	public function getDelivery_status(){
		return $this->delivery_status;
	}
	public function setDelivery_status($value){
		$this->delivery_status = $value;
	}

	public function getPayment_status(){
		return $this->payment_status;
	}
	public function setPayment_status($value){
		$this->payment_status = $value;
	}

	public function getFk_client(){
		return $this->fk_client;
	}
	public function setFk_client($value){
		$this->fk_client = $value;
	}

	public function getFk_PostOffice(){
		return $this->fk_post_office;
	}
	public function setFk_PostOffice($value){
		$this->fk_post_office = $value;
	}
}
?>