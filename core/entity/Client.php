<?php
	class Client
	{
		private $surname;
		private $name;
		private $phone;
		private $password = NULL;

		function __construct() {}
	
		public function setSurname(string $value) {
			$this->surname = $value;
		}
		public function setName(string $value) {
			$this->name = $value;
		}
		public function setPhone(string $value) {
			$this->phone = $value;
		}
		public function setPassword(string $value) {
			$this->password = $value;
		}

		public function getSurname() : string {
			return $this->surname;
		}
		public function getName() : string {
			return $this->name;
		}
		public function getPhone() : string {
			return $this->phone;
		}
		public function getPassword() : string {
			return $this->password;
		}

		public function __toString() : string {
			$string = "Surname: ".$this->surname."<br>\nName: ".$this->name."<br>\nPhone: ".$this->phone."<br>\nPassword: ".$this->password;
			return $string;
		}

		public function toArray() : array {
			return [
				'name' => $this->name,
				'surname' => $this->surname,
				'phone' => $this->phone
			];
		}

		public function fromArray(array $data): void {
			$this->name = $data['name'];
			$this->surname = $data['surname'];
			$this->phone = $data['phone'];
		}
	}
?>