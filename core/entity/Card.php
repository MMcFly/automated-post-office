<?php
	class Card
	{
		private $number;
		private $expiration_date;
		private $cvv;

		function __construct() {}

		public function setNumber(string $value) {
			$this->number = $value;
		}
		public function setExpirationDate(string $value) {
			$this->expiration_date = $value;
		}
		public function setCvv(string $value) {
			$this->cvv = $value;
		}

		public function getNumber() : string {
			return $this->number;
		}
		public function getExpirationDate() : string {
			return $this->expiration_date;
		}
		public function getCvv() : string {
			return $this->cvv;
		}

		public function __toString() : string {
			$string = "Number: ".$this->number."<br>\nExpiration date: ".$this->expiration_date."<br>\nCVV: ".$this->cvv;
			return $string;
		}
	}
?>