<?php
interface IWaybillDAO 
{
	public function createWaybill(Waybill $waybill) : void;
	public function getWaybills($client_id) : array;
	public function createReverseWaybill($waybill_id) : array;
}
?>