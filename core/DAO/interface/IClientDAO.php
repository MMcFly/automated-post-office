<?php
	interface IClientDAO 
	{
		public function register(Client $client) : bool;
		public function login(string $phone, string $password) : bool;
		public function getClient(string $phone) : Client;
		public function remove(string $phone) : bool;
		public function changePassword(string $phone, string $oldPassword, string $newPassword) : bool;
	}
?>