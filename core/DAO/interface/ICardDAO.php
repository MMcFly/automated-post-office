<?php
	interface ICardDAO 
	{
		public function addCard(Card $card, Client $client) : bool;
	}
?>