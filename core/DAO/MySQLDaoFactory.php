<?php
	require_once "core/dao/IDAOFactory.php";
	require_once "core/dao/implementation/ClientDAO.php";
	require_once "core/dao/implementation/CardDAO.php";
	
	class MySQLDAOFactory implements IDAOFactory
	{
		public function getClientDAO() : IClientDAO
		{
			return new ClientDAO();
		}
		public function getCardDAO() : ICardDAO 
		{
			return new CardDAO();
		}
	}
?>