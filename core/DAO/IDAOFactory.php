<?php
	interface IDAOFactory
	{
		public function getClientDAO() : IClientDAO;
		public function getCardDAO() : ICardDAO;
	}
?>