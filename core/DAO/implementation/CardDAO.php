<?php
	require_once "core/connection/MySQLConnection.php";
	require_once "core/dao/interface/ICardDAO.php";
	require_once "core/entity/Card.php";
	require_once "core/builder/CardBuilder.php";

	class CardDAO implements ICardDAO
	{
		private $con;

		public function __construct()
		{
			$this->con = MySQLConnection::getInstance()->getConnection();
		}

		public function addCard(Card $card, Client $client) : bool
		{
			$stmt = $this->con->prepare("SELECT addCard(?,?,?,?)");
			if ($stmt->bind_param("ssss", $card->getNumber(), $card->getExpirationDate(),$card->getCvv(), $client->getPhone()))
			{
				if ($stmt->execute())
					return 1;
				else 
					return 0;
			}
			else
				return 0;
		}
	}
?>