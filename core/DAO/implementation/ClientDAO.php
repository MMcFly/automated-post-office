<?php
	require_once "core/connection/MySQLConnection.php";
	require_once "core/dao/interface/IClientDAO.php";
	require_once "core/entity/Client.php";
	require_once "core/builder/ClientBuilder.php";

	class ClientDAO implements IClientDAO
	{
		private $con;

		public function __construct()
		{
			$this->con = MySQLConnection::getInstance()->getConnection();
		}

		function register(Client $client) : bool
		{
			$stmt = $this->con->prepare("SELECT register(?, ?, ?, ?)");

			if ($stmt->bind_param("ssss", $client->getName(), $client->getSurname(), $client->getPhone(), $client->getPassword()))
			{
				if ($stmt->execute())
					return 1;
				else
					return 0;
			}
			else
				return 0;
		}

		public function login(string $phone, string $password) : bool
		{
			$stmt = $this->con->prepare("SELECT `id` FROM `client` WHERE `phone` = ? AND `password` = ?");
			
			if ($stmt->bind_param('ss', $phone, $password))
			{
				if ($stmt->execute())
				{
					$result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
					if (empty($result))
						return 0;
					else
					{
						return 1;
					}
				}
				else
					return 0;
			}
			else
				return 0;
		}

		public function getClient(string $phone) : Client
		{
			$stmt_client = $this->con->prepare("SELECT `surname`, `name`, `phone` FROM `client` WHERE `phone` = ?");
			if ($stmt_client->bind_param('s', $phone))
			{
				if ($stmt_client->execute())
				{
					$result = $stmt_client->get_result()->fetch_all(MYSQLI_ASSOC)[0];

					$clientBuilder = new ClientBuilder();
					$clientBuilder->setSurname($result['surname']);
					$clientBuilder->setName($result['name']);
					$clientBuilder->setPhone($result['phone']);

					$client = $clientBuilder->build();

					return $client;
				}
				else
					return 0;
			}
			else
				return 0;
		}

		public function changePassword(string $phone, string $oldPassword, string $newPassword) : bool
		{
			$stmt = $this->con->prepare("UPDATE `Client` SET `password` = ? WHERE `phone` = ? AND `password` = ?");

			if ($stmt->bind_param('sss', $newPassword, $phone, $oldPassword))
			{
				if ($stmt->execute())
					return 1;
				else
					return 0;
			}
			else
				return 0;
		}
		
		//TODO: fix DB schema
		public function remove(string $phone) : bool
		{
			$query = "DELETE FROM `Clients` WHERE `phone`= $phone";
			$result = mysqli_query($this->con, $query);

			if ($result)
			{
				echo "<span style='color:blue;'>Пользователь удалён</span>";
			}
			return 0;
		}
	}
?>