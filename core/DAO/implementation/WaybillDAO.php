<?php
require_once 'MySQLConnection.php';
require_once realpath('DAO/WaybillDAO/IWaybillDAO.php');

class WaybillDAO implements IWaybillDAO
{
	private $con;
	public function __construct() {
		$this->con = MySQLConnection::getInstance()->getConnection();
	}

	public function createWaybill(Waybill $waybill) : void
	{
		if (!($stmt = $this->con->prepare("SELECT createWaybill(?,?,?,?,?,?)"))) 
			{
			echo "Не удалось подготовить запрос: (" . $this->con->errno . ") " . $this->con->error;
			}
		else
			{
			 $stmt->bind_param("iissii", $waybill->getDestination_id(), $waybill->getReceiver_id(), $waybill->getDelivery_type(), $waybill->getDelivery_payer(), $waybill->getFk_client(), $waybill->getFk_PostOffice());
			}
		if (!$stmt->execute())
			{
			echo "Не удалось выполнить запрос: (" . $stmt->errno . ") " . 
			$stmt->error;
			}
		else echo "<span style='color:blue;'>Накладная добавлена</span>";
			 unset($con);
	}

	public function getWaybills($client_id) : array 
	{
		$query = "SELECT receiver_for_id(11) AS "Получатель", sender_for_id(1) AS "Отправитель",
		source_for_id(3) AS "Откуда", destination_for_id(4) AS "Куда",
		shipping_price AS "Стоимость доставки", sent_date AS "Дата отправки", arrival_date AS "Дата прибытия",
		delivery_type AS "Тип доставки", delivery_payer AS "Плательщик доставки", delivery_status AS "Статус доставки",
		payment_status AS "Статус оплаты"
		FROM Client
		JOIN Waybill ON (client.id = waybill.fk_client)
		JOIN Post_office ON (waybill.fk_post_office = post_office.id)
		WHERE receiver_id = ?;";
		$result = mysqli_query($this->con, $query);

		if($result)
		{
			$rows = mysqli_num_rows($result);

			for ($i = 0 ; $i < $rows ; ++$i)
    		{
        		$row = mysqli_fetch_row($result);
        		echo "<tr>";
            		for ($j = 0 ; $j < 8 ; ++$j) echo "<td>$row[$j]</td>";
        			echo "</tr>";
    		}
   			 echo "</table>";
		}
		mysqli_free_result($result);s
	}
}
?>