<?php
	switch ($urn_parts[0])
	{
		case 'sign-up':
			require_once "form/register.php";
			break;
		case 'sign-in':
			require_once "form/login.php";
			break;
		case 'change-password':
			require_once "form/changePassword.php";
			break;
		case 'logout':
			unset($_SESSION["user"]);
			unset($_SESSION["isLogined"]);
			header("Location: /sign-in");
			break;
		case 'addCard':
			if ($_SESSION['isLogined'])
			{
				require_once "form/addCard.php";
			}
			break;
	}
?>