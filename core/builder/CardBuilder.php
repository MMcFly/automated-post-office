<?php
	require_once "core/entity/Card.php";

	class CardBuilder
	{
		private $card;

		function __construct()
		{
			$this->card = new Card();
			return $this;
		}

		public function setCard(string $number, string $exp_date, string $cvv) {
			$this->card->setNumber($number);
			$this->card->setExpirationDate($exp_date);
			$this->card->setCvv($cvv);
			return $this;
		}
		
		public function build() : Card {
			return $this->card;
		}
	}
?>