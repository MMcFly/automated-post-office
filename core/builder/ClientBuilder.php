<?php
	require_once "core/entity/Client.php";

	class ClientBuilder 
	{
		private $client;

		function __construct()
		{
			$this->client = new Client();
			return $this;
		}

		function setClient(string $name, string $surname, string $phone, string $password) 
		{
			$this->client->setSurname($surname);
			$this->client->setName($name);
			$this->client->setPhone($phone);
			$this->client->setPassword($password);
			return $this;
		}

		public function setSurname($surname) {
			$this->client->setSurname($surname);
			return $this;
		}
		public function setName($name) {
			$this->client->setName($name);
			return $this;
		}
		public function setPhone($phone) {
			$this->client->setPhone($phone);
			return $this;
		}
		public function setPassword($password) {
			$this->client->setPassword($password);
			return $this;
		}

		public function build() : Client {
			return $this->client;
		}
	}
?>