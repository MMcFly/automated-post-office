<?php
	require_once "core/config.php";

	class MySQLConnection
	{
		private static $con = null;

		private function __construct()
		{
			$this->con = new mysqli(DB_MYSQL_HOST, DB_MYSQL_USER, DB_MYSQL_PASSWORD, DB_MYSQL_DB);
			if ($this->con->connect_errno)
			{
				die("Не удалось подключиться к MySQL: (" . $this->con->connect_errno . ") " .$this->con->connect_error);
			}
		}

		private function __clone () {}
		private function __wakeup () {}

		public static function getInstance()
		{
			if (is_null(self::$con))
			{
				self::$con = new self;
			}
			return self::$con;
		}

		public function getConnection()
		{
			return $this->con;
		}
	}
?>