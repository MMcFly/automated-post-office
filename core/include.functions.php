<?php
	function includeNavigation()
	{
		if ($_SESSION["isLogined"])
		{
			include_once "pages/parts/navigation.logined.phtml";
		}
		else
			include_once "pages/parts/navigation.phtml";
	}
?>