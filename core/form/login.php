<?php
	require_once "core/dao/MySQLDAOFactory.php";

	if (isset($_POST['signIn']))
	{
		$phone = htmlspecialchars($_POST['phone']);
		$password = hash('sha256', htmlspecialchars($_POST['password']));

		$DAOFactory = new MySQLDAOFactory();
		$clientDAO = $DAOFactory->getClientDAO();
		$isLogined = $clientDAO->login($phone, $password);
		if ($isLogined)
		{
			$client = $clientDAO->getClient($phone);
			$_SESSION["isLogined"] = true;
			$_SESSION["user"] = $client->toArray();
		}
		else
			$_SESSION["isLogined"] = false;
			
		header("Location: /");
	}
?>