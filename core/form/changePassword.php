<?php
	require_once "core/dao/MySQLDAOFactory.php";

	if (isset($_POST['changePassword']))
	{
		$phone = htmlspecialchars($_POST['phone']); //stub 
		$oldPassword = hash('sha256', htmlspecialchars($_POST['oldPassword']));
		$newPassword = hash('sha256', htmlspecialchars($_POST['newPassword']));

		$DAOFactory = new MySQLDAOFactory();
		$clientDAO = $DAOFactory->getClientDAO();
		$result = $clientDAO->changePassword($phone, $oldPassword, $newPassword);
		
		echo $result;
	}
?>