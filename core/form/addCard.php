<?php
	require_once "core/dao/MySQLDaoFactory.php";
	require_once "core/builder/ClientBuilder.php";

	if (isset($_POST['addCard']))
	{
		$cardNumber = htmlspecialchars($_POST['creditCard']);
		$expirationDate = htmlspecialchars($_POST['expirationDate']);
		$cvv = htmlspecialchars($_POST['CVV']);

		$client = new Client();
		$client->fromArray($_SESSION['user']);

		$cardBuilder = new CardBuilder();
		$cardBuilder->setCard($cardNumber, $expirationDate, $cvv);
		$card = $cardBuilder->build();

		$DAOFactory = new MySQLDAOFactory();
		$cardDAO = $DAOFactory->getCardDAO();
		$result = $cardDAO->addCard($card, $client);

		if ($result)
		{
			echo "True";
		}
		else
			echo "False";
	}
?>