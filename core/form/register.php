<?php
	require_once "core/builder/ClientBuilder.php";
	require_once "core/dao/MySQLDAOFactory.php";

	if (isset($_POST['signUp']))
	{
		$name = htmlspecialchars($_POST['name']);
		$surname = htmlspecialchars($_POST['surname']);
		$phone = htmlspecialchars($_POST['phone']);
		$password = hash('sha256', htmlspecialchars($_POST['password']));
			
		$clientBuilder = new ClientBuilder();
		$clientBuilder->setClient($name, $surname, $phone, $password);
		$client = $clientBuilder->build();
		// $client = (new ClientBuilder())->setClient($name, $surname, $phone, $password)->build(); // the same

		$DAOFactory = new MySQLDAOFactory();
		$clientDAO = $DAOFactory->getClientDAO();

		$result = $clientDAO->register($client);
		die($result);
	}
?>